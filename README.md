# Terraform-Starter

This starter project contains everything needed to get started creating infrustructure in AWS using the DevOps pipeline.

## What's included

`Jenkinsfile/.gitlab-ci.yml` - A minimal pipeline configuration to deploy to your accounts using the standard PCE Terraform pipeline

`.gitignore` - The Ally default .gitignore with additions for Terraform file types

`backend.tf` - The backend configuration. This tells Terraform where to find and store your projects state. The state is used by Terraform to map real world resources to your configuration and keep track of metadata. State is shared per project and stored in S3.

`providers.tf` - Configures your providers. In this case, sets the AWS region

`versions.tf ` - The versions of terraform and providers required to use this project

`variables.tf` - Variables are declared here. You can set their required types, default values, and descriptions. The default required variables are included.

`terraform.tfvars` - Variable values are defined here. Any variables from `variables.tf` can be set or overriden here

`main.tf ` - The primary file for defining terraform resources. This is where you will include the data and resources to create infrastructure. As your project grows you may reorganize into multiple `.tf` files or modules (folders)

## How to use

Ensure your local terraform environment is set up ([See instructions here](https://confluence.int.ally.com/display/SE/4%29+Developer+-+Onboarding)).

Add your infrustructure resources to `main.tf` then from within the `.aws/terraform` folder in a terminal run `terraform init` to download project dependencies (Make sure your terminal is logged in to AWS). Run `terraform fmt` to fix indentation and `terraform validate` to validate. Commit and push to Bitbucket/Gitlab then run a build of your branch to deploy to AWS.

Ally Terraform Modules: [https://bitbucket.int.ally.com/projects/TF](https://bitbucket.int.ally.com/projects/TF)
The Ally Terraform modules project contains modules for common AWS services and patterns such as an S3 bucket or Lambda function. You should always use modules as a starting point instead of building all the resources yourself. Modules provide baseline configurations and comply with IPRM requirements. If no module is available for a service you want to use, consult the cloud team as the service may not yet be approved for production usage.

AWS Provider Documentation: [https://registry.terraform.io/providers/hashicorp/aws](https://registry.terraform.io/providers/hashicorp/aws)
Ensure you are looking at documentation for the version of the AWS provider you're using. (Specified in the `versions.tf` file)