
'use strict'

const AWS = require('aws-sdk'); 

class SecretsManager {
    
    /**
     * Uses AWS Secrets Manager to retrieve a secret
     */
    static async getSecret (secretName, region){
        console.log("1--");
        const config = { region : region }
        var secret, decodedBinarySecret;
        let secretsManager = new AWS.SecretsManager(config);
        console.log("2--");
        try {
            console.log("3--");
            let secretValue = await secretsManager.getSecretValue({SecretId: secretName}).promise();
            console.log("4--");
            if ('SecretString' in secretValue) {
                console.log("5--");
                return secret = secretValue.SecretString;
            } else {
                console.log("6--");
                let buff = new Buffer(secretValue.SecretBinary, 'base64');
                return decodedBinarySecret = buff.toString('ascii');
            }
        } catch (err) {
            console.log("Secret retrival from AWS failed. Error code is : "+err.code);
            throw err;            
        }
        console.log("7--");
    }    
}
module.exports = SecretsManager;
