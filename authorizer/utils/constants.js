'use strict'

let constants ={
    AUTH_SECRET_NAME : process.env.AUTH_SECRET_NAME
};

module.exports = Object.freeze(constants);