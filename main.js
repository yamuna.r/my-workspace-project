
const expressMain = require('express');
var appMain = expressMain();
const morgan = require('morgan');
var bodyParser = require('body-parser');
appMain.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
appMain.use(bodyParser.json());
appMain.use(bodyParser.urlencoded({
   extended: true
 })); //init routes

 console.log("coming in main file::::::::");

var getCustomer = require('./src/controller/getCustomer.ts');

appMain.use(getCustomer);



 appMain.listen(8080);
 console.log('Node server running on port 8080');

console.log("--------------inside appMain ---------------");

module.exports = appMain;
