'use strict'

let constants ={    
    VENDOR_CONNECT_LAMBDA : process.env.VENDOR_CONNECT_LAMBDA
};

module.exports = Object.freeze(constants);