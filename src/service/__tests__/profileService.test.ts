var customerWsImplMock = require('../../../src/wsImpl/customerWsDaoImpl.ts');
const customerProfileService = require("../../../src/service/profileService.ts");
const httpErrorsMockProfileService = require("../../../src/model/httpError.ts");
const customerWsMockResponse = require("../__fixtures__/wsResponseFixtures.ts");
  describe('should return ws response', () => {
	  let getCustomerWsResponseSpy;

  beforeEach(() => {
    getCustomerWsResponseSpy = jest.spyOn(customerWsImplMock, 'customerwsImpl');
  });	
  afterEach(() => {
    jest.resetAllMocks();
  });	
    it('should return ws response', async () => {
		getCustomerWsResponseSpy.mockReturnValueOnce(customerWsMockResponse.customerWSResponse1);
 const custWsRes = await customerProfileService.parseProfileResponse("9319860");
	
    expect(custWsRes.age).toStrictEqual(62.4);
	 });
	});
	
	
  describe('should return error response', () => {
	  let getCustomerWsResponseSpy;

  beforeEach(() => {
    getCustomerWsResponseSpy = jest.spyOn(customerWsImplMock, 'customerwsImpl');
  });	
  afterEach(() => {
    jest.resetAllMocks();
  });	
    it('should return error response', async () => {
		getCustomerWsResponseSpy.mockReturnValueOnce( new httpErrorsMockProfileService.ResponseNotValidError("USB_BUS_058","System is Unavailable",""));
 const custWsRes = await customerProfileService.parseProfileResponse("9319860");
 console.log("custWsRes::#$@#:$@#$:@#$:@#:$@#:$@#:$:@#:$:@#$:@#:$:@#:$@#:$:@#:$:@#$@#:$@#:$:@#",custWsRes);
	
    expect(custWsRes.errorCode).toEqual('USB_BUS_058');
	 });
	});	
	
	
	
  describe('should return no detail found response', () => {
	  let getCustomerWsResponseSpy;

  beforeEach(() => {
    getCustomerWsResponseSpy = jest.spyOn(customerWsImplMock, 'customerwsImpl');
  });	
  afterEach(() => {
    jest.resetAllMocks();
  });	
    it('should return no detail found response', async () => {
		getCustomerWsResponseSpy.mockReturnValueOnce('<?xml version="1.0" encoding="UTF-8"?>\
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">\
	<soapenv:Body>\
		<IFX xmlns="http://www.ifxforum.org/IFX_150">\
			<SignonRs>\
				<ServerDt>2021-11-18T00:00:00.000000-00:00</ServerDt>\
				<Language>en_US</Language>\
			</SignonRs>\
			<BaseSvcRs>\
			</BaseSvcRs>\
		</IFX>\
	</soapenv:Body>\
</soapenv:Envelope>'
	);
 const custWsRes = await customerProfileService.parseProfileResponse("9319860");
	
    expect(custWsRes.errorCode).toStrictEqual('CUST-GC-E001');
	 });
	});
	
describe('should return ws response', () => {
	  let getCustomerWsResponseSpy;

  beforeEach(() => {
    getCustomerWsResponseSpy = jest.spyOn(customerWsImplMock, 'customerwsImpl');
  });	
  afterEach(() => {
    jest.resetAllMocks();
  });	
    it('should return ws response', async () => {
		getCustomerWsResponseSpy.mockReturnValueOnce(customerWsMockResponse.customerWSResponse2);
 const custWsRes = await customerProfileService.parseProfileResponse("9319860");
	
    expect(custWsRes.age).toStrictEqual(62.4);
	 });
	});