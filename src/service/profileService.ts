
//fs = require('fs');
const { parseStringPromise, processors } = require('xml2js');
const getHttpErrors = require("../model/httpError.ts");
var customerWsImpl = require('../wsImpl/customerWsDaoImpl.ts');
const customer = require("../decorator/customer.ts");

//var infinispan = require('infinispan'); 

//var connected = infinispan.client({ port: 11333, host: 'cashedge-session-svc-master-cache-hotrod.deposits-api.svc' }, { cacheName: 'CASHEDGE-SESSION' });  
var customerJsonResponse;
function delayTime(n) {
    return new Promise(function (resolve) {
        setTimeout(resolve, n * 1000);
    });
}

async function parseProfileResponse(cif) {  

    const options = {
        tagNameProcessors: [processors.stripPrefix],
        explicitArray: false,
        attrkey: 'Nested',
    };

    //const {response} = await customerWsImpl.customerwsImpl();
 //   console.log(response);

     (async () => {
        var response = await customerWsImpl.customerwsImpl(cif);

        console.log("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&",response);
        if (response instanceof getHttpErrors.ResponseNotValidError) {
		console.log("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&************************");
            customerJsonResponse = response;
        } else {
            console.log("coming here:::::::::::!");
            const {
                Envelope: { Body },
            } = await parseStringPromise(response, options);
			
            if (typeof Body.IFX.BaseSvcRs.CustInqRs === 'undefined')
            {
                      console.log("coming here:::::if::::::!",Body.IFX.BaseSvcRs);
                //this is same condition where in DAPI adding status as CUST_GC_E001
                customerJsonResponse= new getHttpErrors.ResponseNotValidError("CUST-GC-E001", "Error retrieving customer information.", '');
            }
            else{
            const statusCode = Body.IFX.BaseSvcRs.CustInqRs.Status.StatusCode;
           // const additionalStatusCode = Body.IFX.BaseSvcRs.CustInqRs.Status.AdditionalStatus[0].StatusCode;
            const statusDescription = Body.IFX.BaseSvcRs.CustInqRs.Status.StatusDesc;
           // console.log("additionalStatusCode::::::::::::::::::::::::::::::::: ", additionalStatusCode);
           // if (additionalStatusCode === '1120') {
                console.log("coming here:::::::::::3",statusCode);
           //     customerJsonResponse =  new getHttpErrors.ResponseNotValidError("CUST-GC-E001", "No Customer found for Input Criteria", '');
           // }
           if (statusCode === '0' && Body.IFX.BaseSvcRs.CustInqRs.CustRec.CustAdditionalInfo !== "") {
                var birthdate = Body.IFX.BaseSvcRs.CustInqRs.CustRec.CustInfo.PersonInfo.BirthDt;
                var etinStatusCodePvtEncrypt = Body.IFX.BaseSvcRs.CustInqRs.CustRec.CustAdditionalInfo.IdInfo.CompositeTINInfo.W8W9CertInfo.CertStatusCode;
                if (Body.IFX.BaseSvcRs.CustInqRs.CustRec.CustAdditionalInfo.CustAcctRelatedInfo.StmtInfo.StmtDeliveryPref === 'Print') {
                    var statementDeliveryPreference = 'Online and Paper';
                } if (Body.IFX.BaseSvcRs.CustInqRs.CustRec.CustAdditionalInfo.CustAcctRelatedInfo.StmtInfo.StmtDeliveryPref === 'Tickler') {
                    var statementDeliveryPreference = 'Online';
                }
                var mothersMaidenName = Body.IFX.BaseSvcRs.CustInqRs.CustRec.CustInfo.PersonInfo.MotherMaidenName;
                var backupWithholdingFlagPvtEncrypt = Body.IFX.BaseSvcRs.CustInqRs.CustRec.CustAdditionalInfo.CustAcctRelatedInfo.BackupWithholding;
                var backupWithholdingReasonCode = Body.IFX.BaseSvcRs.CustInqRs.CustRec.CustAdditionalInfo.CustAcctRelatedInfo.BackupWithholdingReason;
                var ssnPvtblock = Body.IFX.BaseSvcRs.CustInqRs.CustRec.CustAdditionalInfo.IdInfo.CompositeTINInfo.TINInfo.TaxId;
                var accountOpeningDate = Body.IFX.BaseSvcRs.CustInqRs.CustRec.LogInfo.CustAcctOpenDt;
                var segmentCode;
                var restrictInd = Body.IFX.BaseSvcRs.CustInqRs.CustRec.CustAdditionalInfo.CustActivityInfo.RestrictInd;
                var passPhraseQuestion;
                var passPhraseAnswer;
                if (typeof Body.IFX.BaseSvcRs.CustInqRs.CustRec.PhraseInfo !== "undefined") {
                    passPhraseQuestion = Body.IFX.BaseSvcRs.CustInqRs.CustRec.PhraseInfo.PhraseQuestion;
                    passPhraseAnswer = Body.IFX.BaseSvcRs.CustInqRs.CustRec.PhraseInfo.PhraseAnswer;
                }

                var finalResponse = new customer.Customer(birthdate, etinStatusCodePvtEncrypt, statementDeliveryPreference, mothersMaidenName, backupWithholdingFlagPvtEncrypt, backupWithholdingReasonCode, ssnPvtblock,
                    accountOpeningDate, segmentCode, restrictInd, passPhraseQuestion, passPhraseAnswer);

                customerJsonResponse = finalResponse;
            }
	        	}

        }

        

    })()
       await delayTime(1);

     return customerJsonResponse;



};

module.exports = { parseProfileResponse };





