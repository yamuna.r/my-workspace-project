var customerServiceMock = require('../../../src/service/profileService.ts');
const getCustomerMock = require("../../../main");
const httpErrorsMock = require("../../../src/model/httpError.ts");
var validationMock = require('../../../src/bean/headerValidation.ts');
const customerServiceMockResponse = require("../__fixtures__/serviceResponseFixtures.ts");
const request = require('supertest');

	

	
describe('should return error response', () => {
	  let serviceResponseSpy;
  beforeEach(() => {
    serviceResponseSpy = jest.spyOn(customerServiceMock, 'parseProfileResponse');
  });	
  afterEach(() => {
    jest.resetAllMocks();
  });	
    it('status code should be 503', async () => 

{
	//const express = require('express');
	
		serviceResponseSpy.mockReturnValueOnce(new httpErrorsMock.ResponseNotValidError("CUST-GC-E001", "Error retrieving customer information.", ''));

	const res = await request(getCustomerMock).get('/customer')
	.set('CIF','9319860')
	.set('TraceID','9319860')
	.set('ApplicationId', 'ALLYUSBOLB')
	.set('ApplicationName', 'AOB')
	.set('ApplicationVersion', '1.0')
	 expect(res.status).toBe(503);
	 
	});
	
	});
	
	
	
describe('should return error response', () => {
	  let serviceResponseSpy;
  beforeEach(() => {
    serviceResponseSpy = jest.spyOn(customerServiceMock, 'parseProfileResponse');
  });	
  afterEach(() => {
    jest.resetAllMocks();
  });	
    it('status code should be 200', async () => 

{
	//const express = require('express');
	
		serviceResponseSpy.mockReturnValueOnce(customerServiceMockResponse.serviceJSONResponse);

	const res = await request(getCustomerMock).get('/customer')
	.set('CIF','9319860')
	.set('TraceID','9319860')
	.set('ApplicationId', 'ALLYUSBOLB')
	.set('ApplicationName', 'AOB')
	.set('ApplicationVersion', '1.0')
	 expect(res.status).toBe(200);
	 expect(res.text).toBe("{\"customer\":\"{\\\"customer\\\":{\\\"age\\\":62.4,\\\"etinStatusCodePvtEncrypt\\\":\\\"Certified\\\",\\\"statementDeliveryPreference\\\":\\\"Online\\\",\\\"mothersMaidenName\\\":\\\"MOM\\\",\\\"backupWithholdingFlagPvtEncrypt\\\":\\\"FALSE\\\",\\\"backupWithholdingReasonCode\\\":\\\"Voluntary\\\",\\\"ssnPvtblock\\\":\\\"520-78-9621\\\",\\\"accountOpeningDate\\\":\\\"2019-11-08T00:00:00.000Z\\\",\\\"restrictInd\\\":\\\"0\\\"}}\"}");

	 });
	});
	
	
	
  describe('should return error response', () => {
	  let validationResponseSpy;
	var arrayOfErrors = [];
	arrayOfErrors.push(new httpErrorsMock.BadRequestError("USB_COM_505", 'CIF is either empty or contains only spaces','CIF'));
  beforeEach(() => {
    validationResponseSpy = jest.spyOn(validationMock, 'validationErrors');
  });	
  afterEach(() => {
    jest.resetAllMocks();
  });	
    it('status code should be 400', async () => 

{
	//const express = require('express');
	
		validationResponseSpy.mockReturnValueOnce(arrayOfErrors);

	const res = await request(getCustomerMock).get('/customer');
	 expect(res.status).toBe(400);

	 });
	});
	
	
	
	