

const expressGetCustomer = require('express');

const routerGetCustomer = expressGetCustomer.Router();

const requestGetCustomer = require("request");
var validation = require('../bean/headerValidation.ts');
var session = require('express-session');
const httpErrorsGetCustomer = require("../model/httpError.ts");
const profileService = require("../service/profileService.ts");
const expressError = require('../middlewares/error.ts');
//const config = require("config");
var apps = expressGetCustomer();
function delayTimec(n) {
    return new Promise(function (resolve) {
        setTimeout(resolve, n * 1000);
    });
}
console.log("coming into getcustomer:::::uper::::::::::::::::::::::::::::::::::::::::::::");
const validator = require('../middlewares/validator.ts');
routerGetCustomer.get('/customer', (req, res) => {
    console.log("coming into getcustomer:::::::::::::::::::::::::::::::::::::::::::::::::");
    // assumes bearer token is passed as an authorization header
                var validateHeaders = validation.validationErrors(req);
                console.log(validateHeaders.length)
                if (validateHeaders.length > 0) {
                   expressError(validateHeaders, req, res, null);
                    res.end();
                }
                else {
                    (async () => {
						
                        var response = await profileService.parseProfileResponse(req.get('CIF'));
                        if (response instanceof httpErrorsGetCustomer.ResponseNotValidError) {
						console.log("response in instanceof condition ::::::: ", response);	
                            res.status(503).json({ "errors": response });
                            res.end();
                        }else{
                        console.log("response in controller ::::::: ", response);
                        res.status(200).json({ "customer": response });
                        res.end();
						}
                    })()
                }

    });
        
apps.use(expressError);
module.exports = routerGetCustomer;
