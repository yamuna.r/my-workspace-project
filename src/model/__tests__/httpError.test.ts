const httpErrorMock= require("../src/model/httpError.ts");


describe("initialize Account http unauthorized error",()=>{
    it('initialize Account http unauthorized error',()=>{

       var authorizeError= new httpErrorMock.
        UnauthorizedError("401", "unauthorized error.", '')
        expect(authorizeError.errorCode).toBe("401");
        expect(authorizeError.errorDescription).toBe("unauthorized error.");
    })
});

describe("initialize Account http responseNotValiderror",()=>{
    it('initialize Account http responseNotValiderror error',()=>{

       var resourceNotFoundError= new httpErrorMock.
       ResponseNotValidError("CUST-GC-E001", "Error retrieving customer information.", '');
        expect(resourceNotFoundError.errorCode).toBe("CUST-GC-E001");
        expect(resourceNotFoundError.errorDescription).toBe("Error retrieving customer information.");
    })
});

describe("initialize Account http badRequestError",()=>{
    it('initialize Account http badRequestError',()=>{

       var resourceNotFoundError= new httpErrorMock.
       BadRequestError("USB_COM_505", "CIF is empty.", '');
        expect(resourceNotFoundError.errorCode).toBe("USB_COM_505");
        expect(resourceNotFoundError.errorDescription).toBe("CIF is empty.");
    })
});