const headerValidation = require("../src/bean/headerValidation.ts");
const httpErrorsMockHeaderValidation = require("../src/model/httpError.ts");
const mockRequest = require('express');



describe('Incase of header parameters is empty', () => {
	
    it('should throw http error',() => {
      var req=mockRequest();   
      req.set("CIF","");
      req.set("GUID","456789");
      req.set("ApplicationId","");
      req.set("ApplicationName","");
      req.set("ApplicationVersion","");      
      req.set("TraceID","");     
      var arrayOfErrors ;     
      arrayOfErrors= headerValidation.validationErrors(req);       
      var errors=arrayOfErrors[0]; 
      var errorApplicationId=arrayOfErrors[1];
      var errorApplicationName=arrayOfErrors[2];
      var errorApplicationVersion=arrayOfErrors[3];
      var errorTraceID=arrayOfErrors[4];
      expect(errors.errorCode).toEqual('USB_COM_505');
      expect(errors.errorField).toEqual('CIF');

      expect(errorApplicationId.errorField).toEqual('ApplicationId');
      expect(errorApplicationId.errorDescription).toEqual('ApplicationId is either empty or contains only spaces');
      
      expect(errorApplicationName.errorField).toEqual('ApplicationName');
      expect(errorApplicationName.errorDescription).toEqual('ApplicationName is either empty or contains only spaces');

      expect(errorApplicationVersion.errorField).toEqual('ApplicationVersion');
      expect(errorApplicationVersion.errorDescription).toEqual('ApplicationVersion is either empty or contains only spaces');

      expect(errorTraceID.errorField).toEqual('TraceID');
      expect(errorTraceID.errorDescription).toEqual('TraceID is either empty or contains only spaces');

    }
    )
   
});

describe('Incase of ApplicationId enum  invalid', () => {
	
  it('should throw http error',() => {
    var req=mockRequest();   
    req.set("CIF","8890");
    req.set("GUID","456789");
    req.set("ApplicationId","ALLYHH");
    req.set("ApplicationName","AOB");
    req.set("ApplicationVersion","1.0.0");      
    req.set("TraceID","trace");     
    var arrayErrors ;     
    arrayErrors= headerValidation.validationErrors(req);       
   

  }
  )
 
});

