
const expressHeaderValidation = require('express');
const httpError = require("../model/httpError.ts");
const applicationMaps = require("../enum/applicationIdEnum.ts");
const appHeaderValidation = expressHeaderValidation();
const Enum = require('enum');
function validationErrors(req) {
    var arrayOfErrors = [];
    console.log(typeof req.get('CIF'));
    console.log(typeof req.get('GUID'));
    if (typeof req.get('CIF')==="undefined" || req.get('CIF') === "") {
 
        arrayOfErrors.push(new httpError.BadRequestError("USB_COM_505", 'CIF is either empty or contains only spaces','CIF'));
    };
    if (typeof req.get('ApplicationId') === "undefined" || req.get('ApplicationId') === "") {
        console.log("enter into if condition");
        console.log(req.get('ApplicationId'));
        arrayOfErrors.push(new httpError.BadRequestError("USB_COM_505", 'ApplicationId is either empty or contains only spaces', 'ApplicationId'));
    };
    if (typeof req.get('ApplicationName') === "undefined" || req.get('ApplicationName') === "") {
       // console.log(req.get('ApplicationName'));
        arrayOfErrors.push(new httpError.BadRequestError("USB_COM_505", 'ApplicationName is either empty or contains only spaces', 'ApplicationName'));
    };
    if (typeof req.get('ApplicationVersion') === "undefined" || req.get('ApplicationVersion') === "") {
      //  console.log(req.get('ApplicationVersion'));
        arrayOfErrors.push(new httpError.BadRequestError("USB_COM_505", 'ApplicationVersion is either empty or contains only spaces', 'ApplicationVersion'));
    };
    if (typeof req.get('TraceID') === "undefined" || req.get('TraceID') === "") {
      //  console.log(req.get('TraceID'));
        arrayOfErrors.push(new httpError.BadRequestError("USB_COM_505", 'TraceID is either empty or contains only spaces', 'TraceID'));
    };
 //   const applicationIdEnum = new Enum({
   //     'ALLYUSBOLB': "ALLYUSBOLB",
    //    ALLYUSBOLB: "ALLYUSBOLB"
    //})
    var appId = req.get('ApplicationId');
    var appName = req.get('ApplicationName');
    console.log("appName::::::::::::::::::::", appName);
    //console.log("enum value::::::::::::::::::::::::::::::::", applicationIdEnum.appId);
    //console.log("enum value::::::::::::::::::::::::::::::::", applicationIdEnum.ALLYUSBOLB.key);
    console.log("value of applicationId::::::::::::::::::::::::::::: ", applicationMaps.applicationIdMap.get(appId));
    if (typeof applicationMaps.applicationIdMap.get(appId)==="undefined") {
        
        arrayOfErrors.push(new httpError.BadRequestError("USB_COM_503", 'The value for ApplicationId is outside the range of valid values.', 'ApplicationId'));
    };
    console.log("value of applicationName::::::::::::::::::::::::::::: ", applicationMaps.applicationNameMap.get(appName));
    if (typeof applicationMaps.applicationNameMap.get(appName) === "undefined") {

        arrayOfErrors.push(new httpError.BadRequestError("USB_COM_503", 'The value for ApplicationName is outside the range of valid values.', 'ApplicationName'));
    };

    return arrayOfErrors;
};
module.exports = { validationErrors };