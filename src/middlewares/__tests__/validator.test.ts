const mockvalidator = require("../src/middlewares/validator.ts");

describe('/customer call guid and cif not same',()=>{

    it("if condition to check cif and guid are  not same"
    ,()=>{
        const req = { query: { cif: '7738' , guid: '8888'}  };         
        const response={ text :''};        
        const result= mockvalidator.validateParams(req,response);        
        expect(result.cif).toBe("7738");
        expect(result.guid).toBe("8888");
      }
     )
});

describe('/customer call guid and cif are same', ()=>{
    it("if condition to check cif and guid are same"
    ,()=>{
        const req = { query: { cif: '7738' , guid: '7738'}  };         
        const response={ text :''};
        const validatorRes=mockvalidator.validateParams(req,response);
        expect(validatorRes.cif).toBe("7738");
        expect(validatorRes.guid).toBe("7738");
      }
     )

});
