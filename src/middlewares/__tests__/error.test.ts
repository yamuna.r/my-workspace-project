const mockHttpError = require("../../../src/model/httpError.ts");
const error= require('../../../src/middlewares/error.ts');
const mockheaderValidation=require("../../../src/bean/headerValidation.ts");
const expressReq = require('express');



describe("test to check the payload for badrequest",()=>
{
    it('should throw http error',() => {
        var headerReq=expressReq();
        
        headerReq.set("CIF","8890");
        headerReq.set("GUID","456789");
        headerReq.set("ApplicationId","ALLYHH");
        headerReq.set("ApplicationName","AOB");
        headerReq.set("ApplicationVersion","1.0.0");      
        headerReq.set("TraceID","trace");
        var listArrayError;
        listArrayError=mockheaderValidation.validationErrors(headerReq);        
        const req = { text : ''  };   
        const res = {
            send: function(){ },
            json: function(err){
                console.log("\n json error : " + err);               
            },
            status: function(responseStatus) {           
               
                expect(responseStatus).toBe(400);
                return this; 
            }
        }        
        error(listArrayError,req,res);
        
    })
});

describe("test to check the payload for ResourceNotFound",()=>
{
    it('Response code to check in the error payload',() => {
        
        var  resourceError=[];
        resourceError.push(new mockHttpError.
            ResponseNotValidError("CUST-GC-E001", "Error retrieving customer information.", ''));      
            const request = { text : ''  };   
        const response = {
            send: function(){ },
            json: function(errorPayload){
                console.log("\n error payload : " + errorPayload);               
            },
            status: function(responseStatus) {           
               
                expect(responseStatus).toBe(503);
                return this; 
            }
        }        
        error(resourceError,request,response);
        
    })
});