const errorValidation = require("../model/httpError.ts");
module.exports=(error, req, res, next)=> {
    console.log("coming into error.ts");
    if (Array.isArray(error)&& error[0] instanceof errorValidation.BadRequestError) {
        return res.status(400).json({
            errors: [
                error
            ],
        });
    }
    if (error instanceof errorValidation.ResponseNotValidError) {
        return res.status(503).json({
            errors: [
                error
            ],
        });
    }
};

