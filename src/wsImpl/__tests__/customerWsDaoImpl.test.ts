const httpErrorsMockImpl = require("../../../src/model/httpError.ts");
const customerWsDaoImplService = require('../../../src/wsImpl/customerWsDaoImpl.ts');
const constantproperties = require('../../../src/utils/constants');
const AWSMock=require('aws-sdk');
const axios = require('axios');
const customerImplServiceMockResponse = require("../../../src/service/__fixtures__/wsResponseFixtures.ts");



jest.mock('aws-sdk', () => {
  const mLambda = { invoke: jest.fn() };
  return { Lambda: jest.fn(() => mLambda) };
});

jest.mock('axios'); 
  

describe('FIS success response', () => {  
  	
  it('success: should call FIS endpoint successfully', async () => {
    const mLambda = new AWSMock.Lambda({region: 'us-east-1'});
    const mResult = {
      LogResult:'some-data',
      StatusCode:200,
      Payload:'<wsse:Username>DAOAppUser</wsse:Username>\
      <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">daoapp@1234</wsse:Password>'           
    };
    let params = {
      InvocationType: 'Event',
      LogType: 'Tail',
      FunctionName: 'handler2', // the lambda function we are going to invoke
      Payload: 'payloadstructure'
    };
    (mLambda.invoke as jest.Mocked<any>).mockImplementationOnce((params, callback) => {
      callback(null, mResult);
    });   
    
    axios.post.mockReturnValueOnce(customerImplServiceMockResponse.customerWSResponse1);
    const response= await customerWsDaoImplService.customerwsImpl("77899");     
    expect(mLambda.invoke).toHaveBeenCalledTimes(1);
  })
});

describe('FIS error response', () => {  
  	
  it('Error: should call FIS endpoint successfully', async () => {
    const mLambda = new AWSMock.Lambda({region: 'us-east-1'});
    const mResult = {
      LogResult:'some-data',
      StatusCode:200,
      Payload:'<wsse:Username>DAOAppUser</wsse:Username>\
      <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">daoapp@1234</wsse:Password>'           
    };
    let params = {
      InvocationType: 'Event',
      LogType: 'Tail',
      FunctionName: 'handler2', // the lambda function we are going to invoke
      Payload: 'payloadstructure'
    };
    (mLambda.invoke as jest.Mocked<any>).mockImplementationOnce((params, callback) => {
      callback(null, mResult);
    });   
    
    
    const response= await customerWsDaoImplService.customerwsImpl("77899");     
    console.log("response===&&&",response);
    expect(response).toStrictEqual(new httpErrorsMockImpl.ResponseNotValidError("USB_SYS_058", 'Sorry, our system is temporarily unavailable. Please try again later or call us 24/7 for help.', ''));
  })
});

