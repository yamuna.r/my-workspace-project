const httpErrors = require("../model/httpError.ts");
var AWS = require('aws-sdk');
let constants = require('./../utils/constants');
//AWS.config = new AWS.Config();
//AWS.config.region = "us-east-1";
//AWS.config.accessKeyId = "ASIAZO5AOWEO3UX2NXLK";
//AWS.config.secretAccessKey = "DMoJO4H9x91kumqxMqrEnnpNNkmzQBw+EA9du1dv";
function delay(n) {
    return new Promise(function (resolve) {
        setTimeout(resolve, n * 1000);
    });
}

async function get_source_at(){
    console.log("functionName==",get_source_at);
    var secHeader;
    try {
		var lambda = new AWS.Lambda();
        var functionName = constants.VENDOR_CONNECT_LAMBDA;
        
		var params = {FunctionName: functionName};
        console.log("params==",params);
       
		lambda.invoke(params, function(err, data) {
            console.log("data==",data);
		if (err) console.log(err, err.stack); // an error occurred
        else 
		{
			
			secHeader = Object.values(data)[2];
			console.log("secHeader ::::::::::::::::::::::::::::::",secHeader);
			//secHeader = secHeader.slice(1,-1);
            console.log("inside else==",secHeader);
			let buff = Buffer.from(secHeader, 'base64');  
			secHeader = buff.toString('utf-8');
					
			
			
			
			const sec1 = secHeader.replace(/\\\\,/g, '\\,');
			
			const sec2 = sec1.replace(/\n/g,'');
			
			const sec3 = sec2.replace(/'\t'+/g, '');
			
			secHeader =  sec3;
			
		};  
		})} catch (err) { 
            console.log("inside catch==");
            console.log("security header in catch::::::::::::::::::::::::: ", secHeader);
         }         // successful response
    console.log("secHeader before while::::::::::::::::::::::::::::::",secHeader);
		while(secHeader === undefined) {
          require('deasync').runLoopOnce();
        }


        return secHeader;
};

async function customerwsImpl(cif) {
    console.log("cif=====",cif);
    var numericCif = Number(cif);
    console.log(numericCif);
    console.log(cif);
    var returnResponse = "";
    const axios = require("axios");
    const exec = require('child_process').exec;

    const rqId = Math.random().toString(36).slice(2);

    const clientDate = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString().split('.')[0];

    const custLangPref = 'en_US'
    var secHeader = await get_source_at();
	
	console.log("security header after final::::::::::::::::::::::::: ", secHeader);


	
    const buildFISRequest = () =>
        `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">\
    ${secHeader}<soapenv:Body>\
       <IFX xmlns:com.fnf="http://www.fnf.com/xes" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.ifxforum.org/IFX_150" xmlns:com.fnf.custinq_V2_1="http://www.fnf.com/xes/services/cust/custinq/v2_1">\
          <SignonRq Id="ID000000">\
             <SessKey/>\
             <ClientDt>${clientDate}</ClientDt>\
             <CustLangPref>${custLangPref}</CustLangPref>\
             <ClientApp>\
                <Org>com.Fidelity</Org>\
                <Name>Fidelity Bank</Name>\
                <Version>1.0</Version>\
             </ClientApp>\
             <SuppressEcho>0</SuppressEcho>\
          </SignonRq>\
          <BaseSvcRq Id="ID000001">\
             <RqUID>${rqId}</RqUID>\
             <SPName>com.fnf.xes.PRF</SPName>\
             <com.fnf.custinq_V2_1:CustInqRq>\
                <RqUID>${rqId}</RqUID>\
                <CustId>\
                   <SPName>com.fnf.xes.PRF</SPName>\
                   <CustPermId>${cif}</CustPermId>\
                </CustId>\
             </com.fnf.custinq_V2_1:CustInqRq>\
          </BaseSvcRq>\
       </IFX>\
    </soapenv:Body>\
 </soapenv:Envelope>`;

    const soapHeaders = {
        headers: {
            'Content-Type': 'text/xml',
        },
    };

    
    const soapRequest = buildFISRequest();
    try {
        console.log("coming into try block::::::",soapRequest);
        const { data } = await axios.post("https://dapi-vendor-proxy.ocp-dev.int.ally.com/axis/services/IFXService2", soapRequest, soapHeaders);
        console.log("::::::::: data ::::::::::::::::::::::",  data );
        returnResponse = data;
        
    } catch (err) {
		
        console.log("coming into catch block::::::",err);
        return new httpErrors.ResponseNotValidError("USB_SYS_058", 'Sorry, our system is temporarily unavailable. Please try again later or call us 24/7 for help.', '')
    }

        return returnResponse;
    };





module.exports = { customerwsImpl }
