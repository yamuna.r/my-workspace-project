const customerWSRequest =
`<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">\
secHeader<soapenv:Body>\
   <IFX xmlns:com.fnf="http://www.fnf.com/xes" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.ifxforum.org/IFX_150" xmlns:com.fnf.custinq_V2_1="http://www.fnf.com/xes/services/cust/custinq/v2_1">\
      <SignonRq Id="ID000000">\
         <SessKey/>\
         <ClientDt></ClientDt>\
         <CustLangPref></CustLangPref>\
         <ClientApp>\
            <Org>com.Fidelity</Org>\
            <Name>Fidelity Bank</Name>\
            <Version>1.0</Version>\
         </ClientApp>\
         <SuppressEcho>0</SuppressEcho>\
      </SignonRq>\
      <BaseSvcRq Id="ID000001">\
         <RqUID>12345</RqUID>\
         <SPName>com.fnf.xes.PRF</SPName>\
         <com.fnf.custinq_V2_1:CustInqRq>\
            <RqUID>1234</RqUID>\
            <CustId>\
               <SPName>com.fnf.xes.PRF</SPName>\
               <CustPermId>7789</CustPermId>\
            </CustId>\
         </com.fnf.custinq_V2_1:CustInqRq>\
      </BaseSvcRq>\
   </IFX>\
</soapenv:Body>\
</soapenv:Envelope>`;

module.exports = {customerWSRequest};