data "aws_caller_identity" "current" {}

module "namespace" {
  source              = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-name-and-tags.git?ref=v1"
  application_name    = var.application_name
  service_name        = var.service_name
  workspace           = terraform.workspace
  environment         = var.environment
  application_id      = var.application_id
  data_classification = var.data_classification
  issrcl_level        = var.issrcl_level
  owner               = var.owner
  scm_project         = var.scm_project
  scm_repo            = var.scm_repo
  scm_branch          = var.scm_branch
  scm_commit_id       = var.scm_commit_id
  additional_tags     = {
    "tf_starter" = var.creation_date
  }
}

# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-lambda/browse
module "lambda" {
  source        = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v6"
  namespace     = module.namespace.lower_resource_name
  name          = "lambda"
  code          = "../../.serverless/DAPIGetCustomer.zip"
  handler       = "index.handler"
  runtime       = "nodejs14.x"
  vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
  tags          = module.namespace.tags
  short_tags    = module.namespace.short_tags
  timeout       = 600
  execution_policy      = data.aws_iam_policy_document.customers_api_lambda_policies.json
  code_requires_zipping = false
  env_vars = {
    VENDOR_CONNECT_LAMBDA	= lookup(var.vendor_connection_lambda_name, var.environment)
  }
}

# The JSON for the policy above
data "aws_iam_policy_document" "customers_api_lambda_policies" {
  version = "2012-10-17"
  # Allows get-customer boss lambda to invoke get-customer worker lambda
  statement {
    sid     = "AllowLambdaToInvokeAnotherLambda"
    actions = [
      "lambda:InvokeFunction",
      "lambda:InvokeAsync"
    ]
    effect    = "Allow"
    resources = [
      "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:${lookup(var.vendor_connection_lambda_name, var.environment)}", 
      "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:${lookup(var.vendor_connection_lambda_name, var.environment)}:*"
      ]
  }
}



