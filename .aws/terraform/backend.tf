terraform {
  backend "s3" {
    bucket         = "ally-us-east-1-650487181597-tf-states"
    key            = "104154-node-poc/terraform.tfstate"
    dynamodb_table = "ally-us-east-1-650487181597-tf-locks"
    region         = "us-east-1"
    encrypt        = true
  }
}
