# Name & Tagging
application_id      = "104154"
application_name    = "dapi"
service_name        = "get-customer"
owner               = "DAPI"
data_classification = "Confidential"
issrcl_level        = "Medium High"
scm_project         = "allyfinancial/consumer-commercial-banking-technology/deposits-api/services"
scm_repo            = "customer-lambda-poc"

awsacct_cidr_code = {
  default = "010-073-112-000"
  dev     = "010-073-112-000"
  qa      = "010-073-128-000"
  cap     = "010-073-128-000"
  psp     = "010-073-160-000"
  prod    = "010-073-160-000"
}

auth_token_secret_name = {
  default = "dapi-dev-lambda-authorizer-secret"
  dev     = "dapi-dev-lambda-authorizer-secret"
  qa      = "dapi-qa-lambda-authorizer-secret"
  cap     = "dapi-cap-lambda-authorizer-secret"
  psp     = "dapi-psp-lambda-authorizer-secret"
  prod    = "dapi-prod-lambda-authorizer-secret"
}

vendor_connection_lambda_name = {
  default = "dapi-dev-security-header-lambda"
  dev     = "dapi-dev-security-header-lambda"
  qa      = "dapi-qa-security-header-lambda"
  cap     = "dapi-cap-security-header-lambda"
  psp     = "dapi-psp-security-header-lambda"
  prod    = "dapi-prod-security-header-lambda"
}

api_basepath = "/customer"
