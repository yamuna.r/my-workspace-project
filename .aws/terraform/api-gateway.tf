locals {
  vpc_endpoint_id = "vpce-04e79933b40b5b7c5"
}

# API Gateway
# Creates an API gateway used for receiving and sending requests to Lambda.
# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-api-gateway
module "api_gateway" {
  source                 = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-api-gateway.git?ref=v2"
  namespace              = module.namespace.lower_short_name
  name                   = "api"
  environment            = var.environment
  endpoint_configuration = "regional"
  stage                  = "api"
  api_body               = jsonencode(
    {
      "openapi" : "3.0.1",
      "info" : {
        "title" : "API",
        "version" : "1.0"
      },
      "schemes" : [
        "https"
      ],
      "paths" : {
    (var.api_basepath) : {
      "get" : {
        "security": [{
           "AWSAccessAuthorizer": []
         }],
        "responses" : {
          "200" : {
            "description" : "200 response"
          },
          "400": {
            "description": "Bad Request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "500": {
            "description": "Internal Server Error"
          },
          "503": {
            "description": "sevice unavailable"
          }
        },
        "x-amazon-apigateway-integration" : {
          "type" : "AWS_PROXY", # For Lambda proxy integration you can use AWS_PROXY
          "uri" : module.lambda.invoke_arn,
          "httpMethod" : "GET",
          "passthroughBehavior": "when_no_match",
          "responses" : {
            "default" : {
              "statusCode" : 200
            }
          }
        }
      }
    }
  }
      "components": {
        "securitySchemes": {
          "AWSAccessAuthorizer": {
            "type": "apiKey",
            "name": "authorizationToken",
            "in": "header",
            "x-amazon-apigateway-authorizer" : {
              "identitySource": "method.request.header.authorizationToken",
              "authorizerUri": "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/${module.authorizer.arn}/invocations",
              "type": "token",
              "authorizerCredentials": module.invocation_role.arn
              "authorizerResultTtlInSeconds": 0
            },
            "x-amazon-apigateway-authtype": "custom"
          }
        }
      }
  
    }
  )
  tags = module.namespace.tags
}

# Provides the API Gateway permissions to Lambda.
resource "aws_lambda_permission" "api_gateway" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda.name
  principal     = "apigateway.amazonaws.com"

  # The "/*/*/*" portion grants access from any method on any resource
  # within the API Gateway REST API.
  source_arn = "${module.api_gateway.rest_api.execution_arn}/*/*/*"
}

# example authorizer function
module "authorizer" {
  source        = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v7"
  namespace     = module.namespace.lower_resource_name
  name          = "auth"
 
  code          = "../../authorizer/"
  handler       = "index.handler"
  runtime       = "nodejs14.x"
  size          = "s"
  vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
  tags          = module.namespace.tags
  short_tags    = module.namespace.short_tags

  env_vars = {
    ENVIRONMENT     = var.environment
    AUTH_SECRET_NAME	= lookup(var.auth_token_secret_name, var.environment)
  }
}

module "invocation_role" {
  source       = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-iam.git?ref=v3"
  name         = "authorizer"
  namespace    = module.namespace.lower_resource_name
  tags         = module.namespace.tags
  trust_policy = data.aws_iam_policy_document.trust_policy.json
  policy = [
    data.aws_iam_policy_document.permissions_policy.json
  ]
}
 
# allow API Gateway to assume this IAM role
data "aws_iam_policy_document" "trust_policy" {
  statement {
    sid    = ""
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }
  }
}
 
# allow this IAM role to invoke the Lambda Authorizer function
data "aws_iam_policy_document" "permissions_policy" {
  statement {
    actions = [
      "lambda:InvokeFunction",
    ]
    resources = [
     module.authorizer.arn
    ]
  }
}
  
