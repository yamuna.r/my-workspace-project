output "function_arn" {
  value = module.lambda.arn
}

output "environment_url" {
  description = "Apigee API URL. This value is picked up by Gitlab for the environment URL"
  value       = "${module.apigee_proxy.api_url}${var.api_basepath}"
}

