# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-apigee/browse
# The Apigee token must be generated when working locally to plan your configuration.
# The best way to do it is to follow the existing documentation: https://confluence.int.ally.com/CloudOnboarding/on-boarding-guide-ally-aws-cloud-prerequisites/4-developer-onboarding/5-ide-setup/mac-c-set-up-log-in-to-apigee
# And then set an environment variable using "export TF_VAR_apigee_access_token=$APIGEE_ACCESS_TOKEN".
module "apigee_proxy" {
  source         = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-apigee.git//modules/api?ref=v2"
  basepath       = var.api_basepath
  application_id = var.application_id
  display_name   = module.namespace.lower_short_name
  description    = "Apigee proxy created by terraform starter quickstart"
  token          = var.apigee_access_token
  namespace      = module.namespace.lower_short_name
  environment    = var.environment
  bundle_src     = "../../apigee/"
  build_path     = "../../.build_output"
  issrcl_level   = var.issrcl_level
  apiproxy_variables = {
    target_server_host      = "${module.api_gateway.rest_api.id}.execute-api.${var.region}.amazonaws.com"
    ssl_client_auth_enabled = false
    ssl_key_store           = "Ally"
    ssl_key_alias           = "star.api.ally.com"
    api_gateway_path        = element(split(".com/", module.api_gateway.rest_api_stage.invoke_url), 1)
    api_gateway_uri         = element(split("/", var.api_basepath), 1)
  }
}

resource "apigee_product" "product" {
  provider      = apigee
  name          = module.apigee_proxy.name
  approval_type = "auto"
  description   = "Apigee Product."
  api_resources = ["/customer"] # Supports the base path and all sub-URIs
  # See here: http://docs.apigee.com/api-services/content/working-scopes
  scopes  = ["READ"]
  proxies = [module.apigee_proxy.name]
  attributes = {
    access             = module.apigee_proxy.access # limit the visible API products that teams can register for in developer portal
    api_classification = module.apigee_proxy.risk
    api_protect_scope  = module.apigee_proxy.risk
    checkmarx_verified = false
  }
}

# Find more info about these 2 resources in here https://docs.apigee.com/api-platform/publish/portal/developer-teams#understand_teams
# https://github.com/zambien/terraform-provider-apigee/blob/master/apigee/resource_company.go
resource "apigee_company" "team" {
  provider = apigee
  name     = module.apigee_proxy.name
  attributes = {
    DisplayName = module.apigee_proxy.name
  }
}

# https://github.com/zambien/terraform-provider-apigee/blob/master/apigee/resource_company_app.go
resource "apigee_company_app" "team_app" {
  provider     = apigee
  name         = module.apigee_proxy.name
  company_name = apigee_company.team.name
  api_products = [apigee_product.product.name]
}
