variable "region" {
  type        = string
  description = "The target AWS region to deploy to."
  default     = "us-east-1"
}

variable "environment" {
  type        = string
  description = "Env to use for resolving variables"
  default     = "default"
}

variable "application_id" {
  type        = string
  description = "The Ally Application ID of the project."
}

variable "application_name" {
  type        = string
  description = "The Ally Application Name of the project."
}

variable "service_name" {
  type        = string
  description = "The Ally Application service name."
}

variable "data_classification" {
  type        = string
  description = "The Ally data classification."
}

variable "issrcl_level" {
  type        = string
  description = "The Information System Security Risk Classification Level for the resource. This value should come from the ServiceNow application catalog"
}

variable "owner" {
  type        = string
  description = "Team owner of the application"
}

variable "scm_project" {
  type        = string
  description = "The Bitbucket project used to deploy the resource."
}

variable "scm_repo" {
  type        = string
  description = "The Bitbucket repository used to deploy the release."
}

variable "scm_branch" {
  type        = string
  description = "The Bitbucket and tag used to deploy the resource."
  default     = "local-deploy"
}

variable "scm_commit_id" {
  type        = string
  description = "The specific commit that was used to deploy the resource."
  default     = "000000"
}

variable "creation_date" {
  type        = string
  description = "The date and time this project was generated."
  default     = "2022-07-12T04:19:18.844869"
}

variable "awsacct_cidr_code" {
  type        = map
  description = "The CIDR block assigned to the account VPC"
}

variable "auth_token_secret_name" {
  type        = map
  description = "Secret name which is used for fetching auth token from secrets"
}

variable "vendor_connection_lambda_name" {
  type        = map
  description = "Lambda name which is written in java and used to connect to vendor(FIS)"
}

variable "apigee_access_token" {
  type        = string
  description = "The apigee access token our service accounts (one for each ORG) generate for CICD with Apigee Edge SaaS"
}

variable "api_basepath" {
  type        = string
  description = "The base path identified for API Products in the API Product Domain directly inform the base path for your Apigee proxy"
}
