terraform {
  required_version = ">= 0.13"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.0, < 5.0.0"
    }
    apigee = {
      source  = "terraform-registry.services.ally.com/ally/apigee"
      version = ">= 0.0.24"
    }
  }
}