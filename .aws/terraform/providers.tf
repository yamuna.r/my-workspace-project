provider "aws" {
  region = var.region
}

provider "apigee" {
  org = module.apigee_proxy.organization
}